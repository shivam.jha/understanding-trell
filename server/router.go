package server

import (
	"gitlab.com/shivam.jha/understanding-trell/controllers"

	"github.com/gin-gonic/gin"
)

var Router *gin.Engine

func CreateUrlMappings() {
	Router = gin.Default()

	Router.Use(controllers.Cors())
	claps := Router.Group("/api/claps")
	{
		claps.GET("/getclaps", controllers.Getclaps)
		claps.POST("/update/", controllers.Addclaps)
	}

	scheduler := Router.Group("/api/scheduler")
	{
		scheduler.GET("/getSchedule", controllers.Getschedule)
		scheduler.POST("/addevent", controllers.Addevent)
		scheduler.PATCH("/update/:id", controllers.Updateschedule)
	}

	medals := Router.Group("/api/medals")
	{
		medals.GET("/getmedal", controllers.Getmedal)
		medals.PATCH("/update/medal", controllers.UpdateMedal)
	}
}
