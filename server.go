package main

import (
	"os"

	"gitlab.com/shivam.jha/understanding-trell/server"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	// if err != nil {
	// 	panic(err)
	// }
	// defer db.Close()

	// err = db.Ping()
	// if err != nil {
	// 	panic(err)
	// }

	// fmt.Println("Successfully connected!")
	server.CreateUrlMappings()
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	server.Router.Run(":" + port)
}
